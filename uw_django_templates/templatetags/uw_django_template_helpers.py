from django import template

register = template.Library()

import logging

logger = logging.getLogger(__name__)

@register.simple_tag
def alert_styles(tags):
    tag_to_style_override = {
        'error': 'alert-danger',
        'warning': 'alert-warning',
        'info': 'alert-info',
        'debug': 'alert-debug',
        'success': 'alert-success',
    }
    styles = []
    for t in tags.split(' '):
        styles.append('{}'.format(tag_to_style_override.get(t, t)))

    return ' '.join(styles)

@register.inclusion_tag('uw_django/blocks/pagination.html')
def paginate(page, num_pages):
    pages = []
    prev = page - 1 if page > 1 else None
    _next = page + 1 if page < num_pages else None
    if num_pages < 10:
        pages = set(range(1, num_pages + 1))
    else:
        pages = (set(range(1, 4)) | set(range(max(1, page - 2), min(page + 3, num_pages + 1))) | set(range(num_pages - 2, num_pages + 1)))

    def build_list():
        last_page = 0
        for p in sorted(pages):
            if p != last_page + 1: yield -1
            yield p
            last_page = p
 
    return {'page': page, 'prev': prev, 'next': _next, 'pages': build_list()}

@register.simple_tag(takes_context=True)
def add_param(context, key, value):
    r = context.__dict__['request']
    g = r.GET.copy()

    g[key] = value

    return "?{}".format(g.urlencode())
