# UW Django Templates

Based on the UW WCMS Look and Feel to emulate that simply within a django application.

Provides static media and templates for Django, include `django_uw_templates` in INSTALLED\_APPS.

In your `settings.py` you also need to specify the following:

```python
# Django compressor settings
# http://django-compressor.readthedocs.org/en/latest/settings/
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),

COMPRESS_ROOT = '__compress__' # for development, produciton you should read the compressor docs
```

`app/templates/base.html`

```django
{% extends "uw_django/base.html" %}
{% load static %}
{% block title %}App Title{% endblock title %}
{% block favicon %}
        <link rel="icon" type="image/png" href="{% static 'images/favicon.png' %}" />
{% endblock favicon %}
{% block nav_links %}
<li class="nav-item">
    <a class="nav-link" href="{% url 'admin:index' %}">Admin</a>
</li>

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="nav_dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
    <div class="dropdown-menu" aria-labelledby="nav_dropdown">
        <a class="dropdown-item" href="{% url 'view1' %}">A page</a>
        <a class="dropdown-item" href="{% url 'view2' %}">Another page</a>
    </div>
</li>
{% endblock %}
{% block sitename %}<a href="{% url 'home' %}">App Name</a>{% endblock sitename %}
```

Using the templates on an actual page

`app/templates/app/index.html`

```django
{% extends "base.html" %}
{% load post %}
{% block content %}
<p style="font-size: 20px;">Welcome {{ user.first_name|default:user.username }}.  Select a tool from the menu above or <a href="{% url 'logout' %}">logout</a>.</p>
<h1>Recent News</h1>
<div id="posts" style="margin-left: 5px;">
    {% for post in news %}
    <div class="post" style="padding-left: 5px; border-left: 3px solid #ffea30; margin-bottom: 50px;">
        <h2>{{ post.title }}</h2>
        <hr />
        <div class="post-body">
            {{ post.body|markdown }}
        </div>
        <div class="footer text-right">Published: {{ post.date_published }}</div>
    </div>
    {% empty %}
    <em>No recent posts</em>
    {% endfor %}
</div>
{% endblock content %}
```
