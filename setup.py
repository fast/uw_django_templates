try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup
setup(name='uw_django_templates',
    version='0.2',
    description='Shared templates for UW themed sites',
    author='Ryan Goggin',
    author_email='ryan.goggin@uwaterloo.ca',
    url='https://ryangoggin.net',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'django-compressor>=2.4',
        'django-libsass>=0.8',
    ]
)
